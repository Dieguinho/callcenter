package com.almundo.ejercicio.callcenter.entity;

import java.util.List;
import java.util.concurrent.BlockingQueue;

public abstract class Employee implements CallHandler, Runnable, Comparable<Employee> {
	protected BlockingQueue<Employee> idleEmployeesQueue;
	protected List<Call> answeredCalls;
	protected String name;
	protected Call call;
	
	@Override
	public float handleCall(Call callToBeHandled) throws InterruptedException {
		long startTime = System.currentTimeMillis();		
		callToBeHandled.handle();
		return (System.currentTimeMillis() - startTime) / 1000;
	}

	// Handle call and log call duration, then add call to answered calls 
	// list, finally re-enter the idle employees queue
	@Override
	public void run() {
		try {
			System.out.println(this.getName() + " handling call number " + call.getPhoneNumber());
			float callDuration = handleCall(call);
			System.out.println(this.getName() + " finishing call number " + call.getPhoneNumber()
					+ " The call lasted for " + callDuration + " seconds.");
			
			answeredCalls.add(call);
			idleEmployeesQueue.put(this);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// CompareTo method to be used by the PriorityBlockingQueue
	// to order the employees based on their subtype
	@Override
	public int compareTo(Employee employee) {
		final int BEFORE = -1;
		final int EQUAL = 0;
		final int AFTER = 1;

		if (this instanceof Operator) {
			if (!(employee instanceof Operator)) {
				return BEFORE;
			}
		}

		if (this instanceof Supervisor) {
			if (employee instanceof Operator) {
				return AFTER;
			} else if (employee instanceof Director) {
				return BEFORE;
			}
		}

		if (this instanceof Director) {
			if (!(employee instanceof Director)) {
				return AFTER;
			}
		}

		return EQUAL;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Call getCall() {
		return call;
	}

	public void setCall(Call call) {
		this.call = call;
	}

}
