package com.almundo.ejercicio.callcenter.entity;

import java.util.List;
import java.util.concurrent.BlockingQueue;

public class Operator extends Employee{
	
	public Operator(String name, BlockingQueue<Employee> queue, List<Call> answeredCalls) {
		this.name = name;
		this.idleEmployeesQueue = queue;
		this.answeredCalls = answeredCalls;
	}	
	
}
