package com.almundo.ejercicio.callcenter.entity;

public interface CallHandler {
	public float handleCall(Call callToBeHandled) throws InterruptedException;
}
