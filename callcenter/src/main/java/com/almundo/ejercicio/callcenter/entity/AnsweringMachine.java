package com.almundo.ejercicio.callcenter.entity;

public class AnsweringMachine implements CallHandler{

	@Override
	public float handleCall(Call waitingCall) {
		System.out.println("All our representatives are busy right now, please try again in a few minutes.");
		return 0f;
	}

}
