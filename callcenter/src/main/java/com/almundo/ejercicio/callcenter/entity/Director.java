package com.almundo.ejercicio.callcenter.entity;

import java.util.List;
import java.util.concurrent.BlockingQueue;

public class Director extends Employee{
	
	public Director(String name, BlockingQueue<Employee> queue, List<Call> answeredCalls) {
		this.name = name;
		this.idleEmployeesQueue = queue;
		this.answeredCalls = answeredCalls;
	}
}
