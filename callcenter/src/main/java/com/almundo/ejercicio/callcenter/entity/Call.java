package com.almundo.ejercicio.callcenter.entity;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Call {
	private int phoneNumber;
	private boolean processed = false;
	
	public int getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isProcessed() {
		return processed;
	}

	public void handle() {
		int seconds = new Random().nextInt(11 - 5) + 5;
		
		try {
			TimeUnit.SECONDS.sleep(seconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
		
		 processed = true;
	}
}
