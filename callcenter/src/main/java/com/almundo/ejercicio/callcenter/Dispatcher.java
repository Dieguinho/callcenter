package com.almundo.ejercicio.callcenter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

import com.almundo.ejercicio.callcenter.entity.Call;
import com.almundo.ejercicio.callcenter.entity.Director;
import com.almundo.ejercicio.callcenter.entity.Employee;
import com.almundo.ejercicio.callcenter.entity.Operator;
import com.almundo.ejercicio.callcenter.entity.Supervisor;

// Singleton that manages the pool of Employees and the queue of phone calls
public class Dispatcher {
	private volatile static Dispatcher dispatcher;
	private DispatcherThread dispatcherThread;
	private BlockingQueue<Call> callQueue;
	private BlockingQueue<Employee> idleEmployeesQueue;
	private List<Call> answeredCalls;
	
	private Dispatcher() {
		this.callQueue = new LinkedBlockingQueue<>();
		this.idleEmployeesQueue = new PriorityBlockingQueue<>();
		this.answeredCalls = new ArrayList<>();
		
		// Initialize Idle Employees Queue
		try {
			Employee employee;
			employee = new Director("Director", idleEmployeesQueue, answeredCalls);
			this.idleEmployeesQueue.put(employee);
			
			for (int i = 0; i < 3; i++) {
				employee = new Supervisor("Supervisor" + i, idleEmployeesQueue, answeredCalls);
				this.idleEmployeesQueue.put(employee);
			}
			
			for (int i = 0; i < 10; i++) {
				employee = new Operator("Operator" + i, idleEmployeesQueue, answeredCalls);
				this.idleEmployeesQueue.put(employee);
			}
			
			dispatcherThread = new DispatcherThread();
			new Thread(dispatcherThread).start();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static Dispatcher getInstance() {
		if (dispatcher == null) {
			synchronized (Dispatcher.class) {
				if (dispatcher == null) {
					dispatcher = new Dispatcher();
				}
			}
		}

		return dispatcher;
	}

	public synchronized void dispatchCall(Call call) {
		try {
			callQueue.put(call);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public List<Call> getAnsweredCalls() {
		return answeredCalls;
	}

	public void setAnsweredCalls(List<Call> answeredCalls) {
		this.answeredCalls = answeredCalls;
	}

	// Thread that blocks listening to new phone call arrivals and then blocks 
	// on employees for the first available employee to handle the call
	private class DispatcherThread implements Runnable {
		private void dispatchCall(Call call, Employee employee) {
			employee.setCall(call);
			new Thread(employee).start();
		}
		
		// While the thread doesn't find a call with phoneNumber -1 keep 
		// looping and handling calls arriving at callQueue
		@Override
		public void run() {
			try {
				for(Call call = callQueue.take();
						 call.getPhoneNumber() != -1;
						 call = callQueue.take()) {
					Employee employee = idleEmployeesQueue.take();
					dispatchCall(call, employee);
				}
				System.out.println("Last Call received, ending DispatcherThread.");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
}
