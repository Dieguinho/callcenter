package com.almundo.ejercicio.callcenter;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.almundo.ejercicio.callcenter.entity.Call;

/**
 * Unit test for Call Center App.
 */
public class AppTest {
	final int amountOfCallsToGenerate = 50;
	final Dispatcher dispatcher = Dispatcher.getInstance();

	// Timeout set to smallest call duration multiplied by amount of calls
	// Multithreading should process all calls much faster
	@Test(timeout = amountOfCallsToGenerate * 5 * 1000)
	public void shouldRunAllThreadsInSmallAmountOfTime() {
		for (int i = 0; i < amountOfCallsToGenerate; i++) {
			final int count = i;
			new Thread(new Runnable() {

				@Override
				public void run() {
					Call call = new Call();
					call.setPhoneNumber(1100000000 + count);
					System.out.println("Enqueuing call number: " + call.getPhoneNumber());
					dispatcher.dispatchCall(call);
				}
			}).start();
		}

		// Send a call with phoneNumber -1 to notify end of calls
		try {
			Thread.sleep(1000);
			Call endCall = new Call();
			endCall.setPhoneNumber(-1);
			dispatcher.dispatchCall(endCall);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		// Loop indefinitely until timeout has reached or until all calls
		// have been answered (polling each second answeredCalls list).
		// If amount of answered calls isn't equal to amount of calls
		// generated then fail by timeout
		// Else assert if all calls were processed (processed equals true).
		List<Call> answeredCalls = dispatcher.getAnsweredCalls();
		while (true) {
			try {
				if (answeredCalls.size() == amountOfCallsToGenerate) {
					for (int i = 0; i < amountOfCallsToGenerate; i++) {
						Call call = answeredCalls.get(i);
						assertTrue(call.isProcessed());
					}
					break;
				}
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	// Blocking
	@Test(expected = NullPointerException.class)
	public void shouldThrowNullPointerExceptionWhenAddedNullElement() {
		Call endCall = null;
		dispatcher.dispatchCall(endCall);
	}
}
